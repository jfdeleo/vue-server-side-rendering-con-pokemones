const Vue = require('vue')
const server = require('express')()
const renderer = require('vue-server-renderer').createRenderer()

// Para usar jquery ajax
var jsdom = require("jsdom");
const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;

var $ = jQuery = require('jquery')(window);

server.get('*', (req, res) => {
  const app = new Vue({
    data: {
      url: req.url,
      pokemon: "",
      especie: "",
      textoEspaniol: ""
    },
    template: `<div>
    <div>Hola soy <font v-bind:color=especie.color.name> {{ pokemon.name }} </font> !</div>
    <img v-bind:src=pokemon.sprites.front_default></img>
    <div>{{ textoEspaniol }}</div> 
    </div>
    `,
    created: function () {
      console.log("-----------------------------------")
      console.log("Created")
      console.log("Despues de este hook el componente se compila el atributo template ")
      console.log("----------------------------------")
      self = this
      $.ajax({
        url: "https://pokeapi.co/api/v2/pokemon" + req.url + "/",
        contentType: "application/json",
        async: false,
        success: function (res) {
          self.pokemon = res
          console.log("La url" + self.url)
          console.log("A quien encontré fue a " + res.name)
        }
      })
      $.ajax({
        url: "https://pokeapi.co/api/v2/pokemon-species" + req.url + "/",
        contentType: "application/json",
        async: false,
        success: function (res) {
          self.especie = res
          self.especie.flavor_text_entries.forEach(function (entrada) {
            if (entrada.language.name == "es" && self.textoEspaniol == "") {
              self.textoEspaniol = entrada.flavor_text
              return
            }
          })


        }
      })

    },
    beforeMount: function () {
      console.log("-----------------------------------")
      console.log("beforeMount")
      console.log("----------------------------------")
    },
    Mounted: function () {
      console.log("-----------------------------------")
      console.log("Mounted")
      console.log("----------------------------------")

    }
  })
  
  renderer.renderToString(app, (err, html) => {
    if (err) {
      res.status(418).end('Ah,sos gracioso')
      return
    }

    res.end(`
      <!DOCTYPE html>
      <html lang="en">
        <head><title>Techie</title><meta charset="UTF-8"></head>
        <body>${html}</body>
      </html>
    `)
  })
  
})

server.listen(8080)



